const tmi = require("tmi.js");
require("dotenv").config();
const LifxClient = require('lifx-lan-client').Client;

const lifx = new LifxClient();
lifx.init();

const bot = new tmi.Client({
  options: { debug: false },
  connections: {
    reconnect: true,
    secure: true
  },
  identity: {
    username: process.env.BOT_USERNAME,
    password: process.env.TWITCH_TOKEN
  },
  channels: [process.env.CHANNELS], // TODO: Figure out passing more than 1 channel
});

// Register event handlers
bot.on("message", onMessageHandler);
bot.on("connected", onConnectedHandler);
bot.on("join", onJoinHandler);
bot.on("subscription", onSubscriberHandler);
bot.on("resub", onResubHandler);
bot.on("raid", onRaidHandler);


bot.connect();

function onConnectedHandler(addr, port) {
  console.log(`* Connected to ${addr}:${port}`);
}

function onJoinHandler(channel, username, self) {
  const joinMsg = [
    'To the tune of Mighty Mouse: Here I come to wreck the day!',
    'Yo yo yo, phrakbot is in the house!',
    'What is thy bidding master?',
    'Hey look! it\'s the knock-off themichaeljolley',
    '* Actual fun may vary'
  ];

  if (self) {
    bot.say(channel, `${joinMsg[Math.floor(Math.random() * joinMsg.length)]}`);
    return;
  }

  return;
}

function onRaidHandler(channel, username, viewers) {
  bot.say(channel, `Holy Smokes, we're being raided by ${username} and ${viewers} viewers`);
  return;
}

function onSubscriberHandler(channel, username, method, message, userstate) {
  bot.say(channel, `What the what? ${username} just subscribed! Thank you @${username}`);
  return;
}

function onResubHandler(channel, username, streakMonths, message, userstate, methods) {
  bot.say(channel, `Woah! ${username} just resubscribed! Thank you @${username}`);
  return;
}

function onMessageHandler(channel, user, msg, self) {
  const userCommands = ['help', 'hello', 'roll', 'bulb'];
  const modCommands = ['so',];
  const soUsers = ['phrakberg', 'copperbeardy', 'roberttables'];

  // Ignore bot messages
  if (self) return;

  // Ignore messages that don't start with bang
  if (!msg.startsWith('!')) return;

  // log commands
  console.log(user.username, msg);

  // we got this far, let's break up the message
  let msgPayload = msg.toLowerCase().split(" ");

  // strip bang from msgPayload
  msgPayload[0] = msgPayload[0].substring(1, msgPayload[0].length);
  //console.log(msgPayload[0]});

  // Check for allowed commands
  if (!userCommands.includes(msgPayload[0]) && !modCommands.includes(msgPayload[0])) {
    bot.say(channel,
      `Sorry @${user.username}, The idiot coding me only taught me the following commands: ${userCommands.join(', ')}`
    );
    return;
  }

  // Shoutout Command (Mods Only)
  if (msgPayload[0] === 'so' && msgPayload.length > 1) {
    // check if mod or allowed user
    if (user.mod || soUsers.includes(user.username)) {
      // strip @ sign if accidently included
      if (msgPayload[1].startsWith('@')) {
        msgPayload[1] = msgPayload[1].substring(1, msgPayload[1].length);
      }

      bot.say(channel,
        `Shoutout to @${msgPayload[1]} checkout their channel: https://twitch.tv/${msgPayload[1]}`
      );

      return;
    }

    return;
  }

  // Let's add a help command for new users
  if (msgPayload[0] === 'help') {
    bot.say(channel, `The idiot coding me has taught me the following commands: ${userCommands.join(', ')}`);
  }

  // add the hello command
  if (msgPayload[0] === 'hello') {
    // if just hello
    if (msgPayload.length === 1) {
      bot.say(channel, `yo yo yo @${user.username}`);
      return;
    }

    // if a username is included, 
    // stub for future contact information
    if (msgPayload.length > 1) {
      bot.say(channel, `yo yo yo ${msgPayload[1]}`);
      return;
    }
  }

  // Everyone needs a roll command
  if (msgPayload[0] === 'roll') {
    let choice = 6;

    if (msgPayload.length > 1) {
      choice = parseInt(msgPayload[1]);
    }

    let result = roll(choice);
    bot.say(channel, `You rolled a D${choice} and got ${result}`);
    return;
  }

  // Lets fuck up my chromakey with a lifx bulb
  if (msgPayload[0] === 'bulb') {
    // bulb info
    if (msgPayload.length === 1 || msgPayload[1] === 'help') {
      bot.say(channel, 'The bulb can respond to on, off, or a hex color "#FFF" or "#FFFFFF"');
      return;
    }

    if (msgPayload.length > 1 && msgPayload[1] !== 'help') {
      // Turn the light on
      if (msgPayload[1] === 'on') {
        lifx.lights().forEach(light => {
          bot.say(channel, 'Turning the light on!');
          light.on();
          light.colorRgbHex("#FFF");
        });

        return;
      }

      if (msgPayload[1] === 'off') {
        lifx.lights().forEach(light => {
          bot.say(channel, 'Turning the light off!');
          light.off();
        });

        return;
      }

      if (msgPayload[1].match(/^#([0-9a-fA-F]){3}|([0-9a-fA-F]{6})$/i)) {
        lifx.lights().forEach(light => {
          bot.say(channel, 'Changing the color!');
          light.on();
          light.colorRgbHex(msgPayload[1], 2500);
        });

        return;
      }
    }
  }

  // Project info?
  if (msgPayload[0] === 'project') {
    console.log();
  }
}

function roll(sides) {
  return Math.floor(Math.random() * sides) + 1;
}
